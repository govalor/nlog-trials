﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace NW.Schoology.NLog.DB.con
{
    class Program
    {
        static void Main(string[] args)
        {
            //get current logger
            Logger logger = LogManager.GetLogger("databaseLogger");
            

            logger.Trace("Began executing main.");

            try
            {
                int zero = 0;
                int result = 5 / zero;
            }
            catch(DivideByZeroException e)
            {
                logger.Error(e, "Can't divide by zero.");
            }

            logger.Debug("Debug message after try-catch block");

            int x = 323984879;

            int answer = x / 3298473;

            logger.Warn("Warning: that math is hard.");

            logger.Info("Test with Jake.");

            Console.WriteLine("Program for testing NLog logging.");

            logger.Trace("End of program.");

            

            Console.ReadLine();

        }
    }
}
