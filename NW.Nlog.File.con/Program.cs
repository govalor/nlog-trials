﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace NW.Schoology.Nlog.con
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void testMethod()
        {
            logger.Trace("Sample trace message.");
            logger.Debug("Sample debug message.");
            logger.Info("Sample info message.");
            logger.Warn("Sample warning message.");
            logger.Error("Sample error message.");
            logger.Fatal("Sample fatal message.");
        }

        public static void tryCatchMethod()
        {
            try
            {
                Console.WriteLine("try catch- try block");
            }
            catch (Exception e)
            {
                logger.Error(e, "Try-catch block exception message.");
                throw;
            }
        }

        static void Main(string[] args)
        {
            //can add colored consoles, too: https://github.com/NLog/NLog/issues/1935

            //using NLog
            Console.WriteLine("TestMethod:");
            testMethod();

            Console.WriteLine();
            Console.WriteLine("Try-catch method:");
            tryCatchMethod();

            Console.ReadLine();

        }
    }
}