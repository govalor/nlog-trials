﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Targets;
using NLog.Config;

namespace NW.Nlog.DB.Dynamic.con
{
    class Program
    {
        private static void ConfigureLogging()
        {
            //create object for configuration
            LoggingConfiguration logConfig = new LoggingConfiguration();

            //create variable for "application" attribute
            //MappedDiagnosticsContext.Set("application", "NW.NLog.DB.Dynamic.con"); //not working correctly, hardcoded below instead of ${application}

            //create database target with included attributes
            DatabaseTarget dbTarget = new DatabaseTarget();
            dbTarget.ConnectionStringName = "NLogConnection";
            dbTarget.CommandText = @"INSERT INTO Schoology.NLog
            (
                [Application],
	            MachineName,
	            SiteName,
	            Logged,
	            [Level],
	            UserName,
	            [Message],
	            Logger,
	            Properties,
	            ServerName,
	            [Port],
	            [Url],
	            Https,
	            ServerAddress,
	            RemoteAddress,
	            CallSite,
	            Exception
            )
            values
            (
	            @application,
	            @machineName,
	            @siteName,
	            @logged,
	            @level,
	            @userName,
	            @message,
	            @logger,
	            @properties,
	            @serverName,
	            @port,
	            @url,
	            @https,
	            @serverAddress,
	            @remoteAddress,
	            @callSite,
	            @exception
            )";

            /*
             * Another way to do the above, is to call the Stored Procedure instead of using straight SQL
             * 
             * dbTarget.CommandText = @"exec Schoology.InsertLog
                        @application,
                        @machineName,
                        @siteName,
                        @logged,
                        @level,
                        @userName,
                        @message,
                        @logger,
                        @properties,
                        @serverName,
                        @port,
                        @url,
                        @https,
                        @serverAddress,
                        @remoteAddress,
                        @callSite,
                        @exception";
             *
             */

            //add parameters to the target
            //dbTarget.Parameters.Add(new DatabaseParameterInfo("@application", "${application}")); //not working correctly, see line 20
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@application", "NW.NLog.DB.Dynamic.con"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@MachineName", "${machinename}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@siteName", "${iss-site-name}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@logged", "${date}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@level", "${level}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@username", "${aspnet-user-identity}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@message", "${message}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@logger", "${logger}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@properties", "${all-event-properties:separator=|}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@serverName", "${aspnet-request:serverVariable=SERVER_NAME}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@port", "${aspnet-request:serverVariable=SERVER_PORT}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@url", "${aspnet-request:serverVariable=HTTP_URL}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@https", "${when:inner=1:when='${aspnet-request:serverVariable=HTTPS}' == 'on'}${when:inner=0:when='${aspnet-request:serverVariable=HTTPS}' != 'on'}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@serverAddress", "${aspnet-request:serverVariable=LOCAL_ADDR}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@remoteAddress", "${aspnet-request:serverVariable=REMOTE_ADDR}:${aspnet-request:serverVariable=REMOTE_PORT}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@callSite", "${callsite}"));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@exception", "${exception:tostring}"));

            /* create logging rule - LogLevel.type is Trace as this specifies the lowest level to be written to the target
             * Levels go Trace, Info, Debug, Warn, Error, Fatal from lowest to highest
             */
            LoggingRule dbRule = new LoggingRule("dbLogger", LogLevel.Trace, dbTarget);
            
            logConfig.LoggingRules.Add(dbRule);

            //set the logging configuration
            LogManager.Configuration = logConfig;
        }

        static void Main(string[] args)
        {
            ConfigureLogging();

            Logger logger = LogManager.GetLogger("dbLogger");

            logger.Trace("Testing: start of program with a Trace log.");

            //doing stuff

            logger.Debug("Testing: Debug log.");

            try
            {
                int zero = 0;
                int result = 5 / zero;
            }
            catch(DivideByZeroException e)
            {
                logger.Error(e, "Can't divide by zero.");
            }

            logger.Fatal("Testing: Fatal log.");

        }
    }
}
