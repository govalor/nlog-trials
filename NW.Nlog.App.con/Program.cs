﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.LayoutRenderers;
using NLog.Config;
using NLog.Targets;
using NLog.Common;

namespace NW.Nlog.App.con
{
    //custom layout renderer
    [LayoutRenderer("MyFirst")]
    public class MyFirstLayoutRenderer : LayoutRenderer
    {
        public string Config1 { get; set; }

        [RequiredParameter] //nlog will through an exception when this property doesn't have a value
        public string Config2 { get; set; }
        
        [DefaultParameter]
        public string Caps { get; set; }

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append("hello universe!");
            Console.WriteLine("Test 1");
        }
    }

    //--------------------------------------------------------------------------------------------------

    //custom target
    [Target("MyFirst")]
    public sealed class MyFirstTarget : TargetWithLayout
    {
        public MyFirstTarget()
        {
            this.Host = "localhost";
        }

        [RequiredParameter]
        public string Host { get; set; }

        protected override void Write(LogEventInfo logEvent)
        {
            string logMessage = this.Layout.Render(logEvent);

            SendMessageToRemoteHost(this.Host, logMessage);
           // Console.WriteLine("Please work test message: " + logMessage);
        }

        private void SendMessageToRemoteHost(string host, string message)
        {
            Console.WriteLine("SendMessage method gets called");
        }
    }

    class Program
    {
        //static void Main(string[] args)
        //{
        //    //target
        //    Target.Register<NW.Nlog.App.con.MyFirstTarget>("MyFirst"); //generic
        //    //Target.Register("MyFirst", typeof(NW.Nlog.App.con.MyFirstTarget)); //dynamic

        //    //layout renderer
        //    LayoutRenderer.Register<NW.Nlog.App.con.MyFirstLayoutRenderer>("MyFirst"); //generic
        //    //LayoutRenderer.Register("MyFirst", typeof(NW.Nlog.App.con.MyFirstLayoutRenderer));

        //    //supposed to be a layout but idk??

        //    Logger logger = LogManager.GetLogger("consoleLogger");
        //    Logger log2 = LogManager.GetLogger("myFirstLogger");

        //    logger.Info("Info message test");
        //    log2.Info("I work");

        //    Console.ReadLine();
        //}
    }
}
