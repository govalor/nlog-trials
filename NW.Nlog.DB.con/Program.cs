﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace NW.Nlog.DB.con
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int zero = 0;
                int result = 5 / zero;
            }
            catch(DivideByZeroException ex)
            {
                //get a logger object and log exception using NLog, using target databaseLogger from NLog.config file
                Logger logger = LogManager.GetLogger("databaseLogger");

                //add message to exception
                logger.Error(ex, "Test error.");
            }

            
            Console.WriteLine("Finished");
            Console.ReadLine();
        }
    }
}
